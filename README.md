# Docker

If you want to visualize images or start a GUI-application in docker, you should run with docker
```
docker run -it -d --net=host -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --privileged=true -v /home/youtu:/mnt/local_home univ:local_driver /bin/bash
```

# Error: No protocol specified QXcbConnection: Could not connect to display :0 Aborted (core dumped)
xhost +local:docker
