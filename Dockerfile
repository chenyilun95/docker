FROM nvidia/cuda:8.0-cudnn7-devel
WORKDIR /root

# Get required packages
RUN apt-get update && \
 apt-get install vim \
                 python-pip \
                 python-dev \
                 python-opencv \
                 python-tk \
                 libjpeg-dev \
                 libfreetype6 \
                 libfreetype6-dev \
                 zlib1g-dev \
                 cmake \
                 wget \
                 cython \
                 git \
                 tmux \
                 at \
                 tree \
                 zip \
                 -y

# Get plot tools & latex ?
 apt-get install gnuplot \
                 ghostscript \
                 texlive-extra-utils \
                 imagemagick \
                 python-vtk python-qt4 python-qt4-gl python-configobj


# Get required python modules
# Install PyTorch 0.4 for python3.5 and cuda8
RUN apt-get install python3-pip -y
RUN pip3 install image \
               scipy \
               matplotlib \
               PyYAML \
               numpy \
               easydict \
               tensorflow-gpu==1.4 \
               torch \
               torchvision \
               PyQt5 \
               mayavi

RUN pip3 install shapely \
                 numba \
                 llvmlite
            
# From requirements.txt
RUN pip3 install --upgrade setuptools && \
    pip3 install termcolor>=1.1 \
                tabulate>=0.7.7 \
                tqdm>4.11.1 \
                pyarrow>=0.9.0 \
                pyzmq>=16 \
                Cython>=0.19.2 \
                numpy>=1.7.1 \
                scipy>=0.13.2 \
                scikit-image>=0.9.3 \
                matplotlib>=1.3.1 \
                ipython>=3.0.0 \
                h5py>=2.2.0 \
                leveldb>=0.191 \
                networkx>=1.8.1 \
                nose>=1.3.0 \
                pandas>=0.22.0 \
                python-dateutil \
                protobuf>=2.5.0 \
                python-gflags>=2.0 \
                pyyaml>=3.10 \
                Pillow>=2.3.0 \
                six>=1.1.0 \
                msgpack \
                msgpack_numpy \
                datetime \
                setproctitle \
                easydict \
                opencv-python \
                setuptools \
                mock \
                bpython \
                jupyter \
                cffi \
                virtualenv \
                packaging 


# Add CUDA to the path
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/local/cuda/lib64
ENV CUDA_HOME /usr/local/cuda
RUN ldconfig

# Install caffe2
# First install dependencies
RUN apt-get update && \
 apt-get install -y --no-install-recommends \
          build-essential \
          cmake \
          git \
          libgoogle-glog-dev \
          libgtest-dev \
          libiomp-dev \
          libleveldb-dev \
          liblmdb-dev \
          libopencv-dev \
          libopenmpi-dev \
          libsnappy-dev \
          libprotobuf-dev \
          openmpi-bin \
          openmpi-doc \
          protobuf-compiler \
          python-dev \
          python-pip \
          python3-tk

RUN pip install \
          future \
          numpy \
          protobuf

RUN apt-get install -y --no-install-recommends libgflags-dev

# Clone Caffe2's source code from our Github repository
# Create a directory to put Caffe2's build files in
RUN git clone --recursive https://github.com/pytorch/pytorch.git && cd pytorch && \
    git submodule update --init && \
    mkdir build

# Compile, link, and install Caffe2
WORKDIR /root/pytorch/build
RUN cmake .. && \
    make install

# AutoJump
WORKDIR /root
RUN git clone https://github.com/joelthelion/autojump.git && \
    cd /root/autojump/ && ./install.sh

RUN echo '[[ -s ~/.autojump/etc/profile.d/autojump.bash ]] && . ~/.autojump/etc/profile.d/autojump.bash' >> ~/.bashrc

WORKDIR /root
